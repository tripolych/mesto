// Buttons

const buttonOpenEditProfilePopup = document.querySelector('.profile__edit-button');
const buttonOpenAddCardPopup = document.querySelector('.profile__add-button');

const buttonCloseEditProfilePopup = document.querySelector('.popup__close-button_type_edit-profile');
const buttonCloseAddCardPopup = document.querySelector('.popup__close-button_type_add-card');
const buttonCloseViewPhotoPopup = document.querySelector('.popup__close-button_type_view-photo');

// Popups

const popupEditProfile = document.querySelector('.popup_type_edit-profile');
const popupAddCard = document.querySelector('.popup_type_add-card');
const popupViewPhoto = document.querySelector('.popup_type_view-photo');

// Forms

const popupFormEditProfileElement = popupEditProfile.querySelector('.popup__form_type_edit-profile');
const popupNameEditProfileElement = popupFormEditProfileElement.querySelector('.popup__input_type_name-edit-profile');
const popupDescriptionEditProfileElement = popupFormEditProfileElement.querySelector('.popup__input_type_description-edit-profile');
const popupSubmitButtonEditProfile = popupFormEditProfileElement.querySelector('.popup__submit-button_type_edit-profile');
const profileNameError = popupFormEditProfileElement.querySelector('.profile-name-error');
const profileDescriptionError = popupFormEditProfileElement.querySelector('.profile-description-error');

const popupFormAddCardElement = popupAddCard.querySelector('.popup__form_type_add-card');
const popupNameAddCardElement = popupFormAddCardElement.querySelector('.popup__input_type_name-add-card');
const popupDescriptionAddCardElement = popupFormAddCardElement.querySelector('.popup__input_type_description-add-card');
const popupSubmitButtonAddCard = popupFormAddCardElement.querySelector('.popup__submit-button_type_add-card');
const cardNameError = popupFormAddCardElement.querySelector('.card-name-error');
const cardLinkError = popupFormAddCardElement.querySelector('.card-link-error');

// Profile

const profileName = document.querySelector('.profile__name');
const profileDescription = document.querySelector('.profile__description');

// Cards

const cardsElement = document.querySelector('.cards');
const cardTemplate = document.querySelector('#card-template').content;

//Functions

const openPopup = (popup) => {
  document.addEventListener('keydown', closePopupEsc);

  popup.classList.add('popup_opened');
};

const closePopup = (popup) => {
  document.removeEventListener('keydown', closePopupEsc);

  popup.classList.remove('popup_opened');
};

const closePopupOverlay = (event, popup) => {
  if (event.target.classList.contains('popup_opened')) {
    closePopup(popup);
  }
};

const closePopupEsc = (event) => {
  const openedPopup = document.querySelector('.popup_opened');

  if (event.key === 'Escape') {
    closePopup(openedPopup);
  }
};

const createCard = (data) => {
  const cardElement = cardTemplate.querySelector('.card').cloneNode(true);
  const cardPhoto = cardElement.querySelector('.card__photo');
  const cardTitle = cardElement.querySelector('.card__title');
  const cardLikeButton = cardElement.querySelector('.card__like-button');
  const cardDeleteButton = cardElement.querySelector('.card__delete-button');
  const popupPhoto = popupViewPhoto.querySelector('.popup__photo');
  const popupPhotoDescription = popupViewPhoto.querySelector('.popup__photo-description');

  cardPhoto.src = data.link;
  cardPhoto.alt = data.alt;
  cardTitle.textContent = data.name;

  cardPhoto.addEventListener('click', () => {
    popupPhoto.src = data.link;
    popupPhoto.alt = data.alt;
    popupPhotoDescription.textContent = data.name;

    openPopup(popupViewPhoto);
  });

  cardLikeButton.addEventListener('click', (event) => {
    event.target.classList.toggle('card__like-button_active');
  });

  cardDeleteButton.addEventListener('click', () => {
    cardElement.remove();
  });

  return cardElement;
};

const submitPopupFormEditProfile = (event) => {
  event.preventDefault();

  profileName.textContent = popupNameEditProfileElement.value;
  profileDescription.textContent = popupDescriptionEditProfileElement.value;

  closePopup(popupEditProfile);
};

const submitPopupFormAddCard = (event) => {
  event.preventDefault();

  cardsElement.prepend(createCard({
    name: popupNameAddCardElement.value,
    link: popupDescriptionAddCardElement.value,
    alt: popupNameAddCardElement.value
  }));

  popupFormAddCardElement.reset();

  disableSubmitButton(popupSubmitButtonAddCard, 'popup__submit-button_disabled');

  closePopup(popupAddCard);
};

// Creating initial cards

initialCards.forEach((card) => {
  cardsElement.append(createCard(card));
});

// Opening profile and card popups

buttonOpenEditProfilePopup.addEventListener('click', () => {
  popupNameEditProfileElement.value = profileName.textContent;
  popupDescriptionEditProfileElement.value = profileDescription.textContent;

  hideInputError(popupNameEditProfileElement, profileNameError, 'popup__input_type_error', 'popup__error_visible');
  hideInputError(popupDescriptionEditProfileElement, profileDescriptionError, 'popup__input_type_error', 'popup__error_visible');

  openPopup(popupEditProfile);
});

buttonOpenAddCardPopup.addEventListener('click', () => {
  hideInputError(popupNameAddCardElement, cardNameError, 'popup__input_type_error', 'popup__error_visible');
  hideInputError(popupDescriptionAddCardElement, cardLinkError, 'popup__input_type_error', 'popup__error_visible');

  popupFormAddCardElement.reset();

  openPopup(popupAddCard);
});

// Closing all popups

buttonCloseEditProfilePopup.addEventListener('click', () => {
  closePopup(popupEditProfile);
});

popupEditProfile.addEventListener('mousedown', (event) => {
  closePopupOverlay(event, popupEditProfile);
});

buttonCloseAddCardPopup.addEventListener('click', () => {
  closePopup(popupAddCard);
});

popupAddCard.addEventListener('mousedown', (event) => {
  closePopupOverlay(event, popupAddCard);
});

buttonCloseViewPhotoPopup.addEventListener('click', () => {
  closePopup(popupViewPhoto);
});

popupViewPhoto.addEventListener('mousedown', (event) => {
  closePopupOverlay(event, popupViewPhoto);
});

// Submitting forms

popupFormEditProfileElement.addEventListener('submit', submitPopupFormEditProfile);

popupFormAddCardElement.addEventListener('submit', submitPopupFormAddCard);

// Validation

const validationConfig = {
  formSelector: '.popup__form',
  inputSelector: '.popup__input',
  submitButtonSelector: '.popup__submit-button',
  inactiveButtonClass: 'popup__submit-button_disabled',
  inputErrorClass: 'popup__input_type_error',
  errorClass: 'popup__error_visible'
};

enableValidation(validationConfig);
